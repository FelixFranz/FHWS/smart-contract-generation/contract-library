module.exports = [
    {
        name: "EIP-20: ERC-20 Token Standard",
        description: "A standard interface for tokens. This standard allows the implementation of a standard API for tokens within smart contracts. It provides basic functionality to transfer tokens, as well as allow tokens to be approved so they can be spent by another on-chain third party.",
        url: "https://eips.ethereum.org/EIPS/eip-20",
        incompatibleTemplates: [],
        variants: [
            {
                name: "Default",
                description: "The default implementation of EIP-20, this implementation has a race condition in the approve function",
                implementation: {
                    path: "./EIP/EIP20/EIP20_Default.sol",
                    name: "EIP20_Default"
                },
                test: {
                    path: "./EIP/EIP20/EIP20_Default.js",
                    data: constructorVariable => {
                        return {
                            name: constructorVariable[0],
                            symbol: constructorVariable[1],
                            decimals: constructorVariable[2],
                            totalSupply: constructorVariable[3]
                        }
                    }
                },
                constructor: [
                    {
                        name: "Default constructor",
                        parameters: [
                            {
                                name: "name",
                                type: "string"
                            },
                            {
                                name: "symbol",
                                type: "string"
                            },
                            {
                                name: "decimals",
                                type: "number",
	                            higherequal: 1
                            },
                            {
                                name: "totalSupply",
                                type: "number",
	                            higherequal: 1
                            }
                        ]
                    }
                ]
            },
            {
                name: "Approve by adding allowance",
                description: "Contains a fixed version of the approve function. It is possible to add and remove tokens instead of providing the number of tokens that are allowed to spend.",
                implementation: {
                    path: "./EIP/EIP20/EIP20_ApproveByAddAllowance.sol",
                    name: "EIP20_ApproveByAddAllowance"
                },
	            test: {
		            path: "./EIP/EIP20/EIP20_ApproveByAddAllowance.js",
		            data: constructorVariable => {
                        return {
                            name: constructorVariable[0],
                            symbol: constructorVariable[1],
                            decimals: constructorVariable[2],
                            totalSupply: constructorVariable[3]
                        }
                    }
	            },
                constructor: [
                    {
                        name: "Default constructor",
                        parameters: [
                            {
                                name: "name",
                                type: "string"
                            },
                            {
                                name: "symbol",
                                type: "string"
                            },
                            {
                                name: "decimals",
                                type: "number",
	                            higherequal: 1
                            },
                            {
                                name: "totalSupply",
                                type: "number",
	                            higherequal: 1
                            }
                        ]
                    }
                ]
            },
            {
                name: "Approve by sending current value",
                description: "Contains a fixed version of the approve function. The number of current tokens need to be provided to call this function.",
                implementation: {
                    path: "./EIP/EIP20/EIP20_ApproveBySendingCurrentValue.sol",
                    name: "EIP20_ApproveBySendingCurrentValue"
                },
	            test: {
		            path: "./EIP/EIP20/EIP20_ApproveBySendingCurrentValue.js",
		            data: constructorVariable => {
                        return {
                            name: constructorVariable[0],
                            symbol: constructorVariable[1],
                            decimals: constructorVariable[2],
                            totalSupply: constructorVariable[3]
                        }
                    }
	            },
                constructor: [
                    {
                        name: "Default constructor",
                        parameters: [
                            {
                                name: "name",
                                type: "string"
                            },
                            {
                                name: "symbol",
                                type: "string"
                            },
                            {
                                name: "decimals",
                                type: "number",
	                            higherequal: 1
                            },
                            {
                                name: "totalSupply",
                                type: "number",
	                            higherequal: 1
                            }
                        ]
                    }
                ]
            }
        ]
    },
	{
		name: "EIP-173: ERC-173 Contract Ownership Standard",
		description: "A standard interface for ownership of contracts. This standard allows for the implementation of a standard API for getting the owner address of a contract and transferring contract ownership to a different address.",
		url: "https://eips.ethereum.org/EIPS/eip-173",
        incompatibleTemplates: ["OWN-1: Mortability Standard"],
		variants: [
			{
				name: "Default",
				description: "The default implementation of EIP-173",
				implementation: {
					path: "./EIP/EIP173/EIP173_Default.sol",
					name: "EIP173_Default"
				},
				test: {
					path: "./EIP/EIP173/EIP173_Default.js",
					data: constructorVariable => {}
				},
				constructor: [
                    {
	                    name: "Default constructor",
	                    parameters: []
                    }
                ]
			}
		]
	},
    {
        name: "EIP-1690: Mortability Standard",
        description: "THIS EIP IS NOT OFFICIALLY ACCEPTED RIGHT NOW!!! A standard interface for that allows the owner to destruct the smart contract. For ownership management it uses EIP-173",
        url: "https://github.com/ethereum/EIPs/blob/d4c18eb0d34ddf2f90e0eccb6be09accd7b1f086/EIPS/eip-1690.md",
        incompatibleTemplates: ["EIP-173: ERC-173 Contract Ownership Standard"],
        variants: [
            {
                name: "Default",
                description: "The default implementation of EIP-1690",
                implementation: {
                    path: "./EIP/EIP1690/EIP1690_Default.sol",
                    name: "EIP1690_Default"
                },
                test: {
                    path: "./EIP/EIP1690/EIP1690_Default.js",
                    data: constructorVariable => {}
                },
                constructor: [
                    {
                        name: "Default constructor",
                        parameters: []
                    }
                ]
            }
        ]
    }
];