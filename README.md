# contract library

These are implementations of multiple smart contracts from https://eips.ethereum.org/ that will be merged into one by the generator.

## Installation

1. Install [NodeJS](https://nodejs.org/en/)
1. Install [Truffle](https://truffleframework.com/docs/ganache/quickstart) `npm install`
1. Optional: Install Ethereum Blockchain e.g. with [Ganache](https://truffleframework.com/docs/ganache/quickstart)
1. Clone repository
    ```bash
    git@gitlab.com:FelixFranz/FHWS/smart-contract-generation/contract-library.git
    ```
    
## Build Contract

Just run following command:

```bash
npm run compile
```     

## Build one file source 

This is needed for [Etherscan](https://etherscan.io/) ([How To](https://medium.com/coinmonks/how-to-verify-and-publish-on-etherscan-52cf25312945))
Just run following command:

```bash
npm run compileOneFile
``` 
   
## Test Contract

Just run following command:

```bash
npm run test
```

## Deploy Contract

The `truffle.js` is not configured, so it connects to an local Ethereum network .

```bash
npm run deploy
```

## Interact with Smart Contract

First you need to connect to the Ethereum network using truffle console.
Then you can run generated JavaScript wrapper to interact with the contract.

In following example we aks the Contract if it supports ERC-165.

```bash
npm run develop
Contract.deployed().then(instance => instance.supportsInterface.call("0x01ffc9a7")).then(txt => console.log(txt))
```