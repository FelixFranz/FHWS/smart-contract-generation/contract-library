pragma solidity ^0.4.23;

import "../EIP165/EIP165_Default.sol";
import "./EIP20_Interface.sol";
import "./SafeMath.sol";

/// https://eips.ethereum.org/EIPS/eip-20
/// based on implementation of OpenZeppelin: https://github.com/OpenZeppelin/openzeppelin-solidity/blob/master/contracts/token/EIP20/EIP20.sol
contract EIP20_Default is EIP165_Default, EIP20_Interface {
    using SafeMath for uint256;

    string private _name;
    string private _symbol;
    uint8 private _decimals;
    mapping (address => uint256) private _balances;
    mapping (address => mapping (address => uint256)) internal _allowed;
    uint256 private _totalSupply;

    constructor(string name, string symbol, uint8 decimals, uint256 totalSupply) internal {
        _name = name;
        _symbol = symbol;
        _decimals = decimals;
        _totalSupply = totalSupply;
        _balances[msg.sender] = totalSupply;

        //pre-calculated interfaceHashes are used to reduce gas
//        registerInterface(
//            calculateFunctionHash("name()") ^
//            calculateFunctionHash("symbol()") ^
//            calculateFunctionHash("decimals()") ^
//            calculateFunctionHash("totalSupply()") ^
//            calculateFunctionHash("balanceOf(address)") ^
//            calculateFunctionHash("transfer(address,value)") ^
//            calculateFunctionHash("transferFrom(address,address,value)") ^
//            calculateFunctionHash("approve(address,uint256)") ^
//            calculateFunctionHash("allowance(address,address)"));
        registerInterface(0x37f66e14);
    }

    /***
    * @return name of the tokens
    */
    function name() external view returns (string){
        return _name;
    }

    /***
    * @return symbol of the tokens
    */
    function symbol() external view returns (string){
        return _symbol;
    }

    /***
    * @return decimals of the tokens
    */
    function decimals() external view returns (uint8){
        return _decimals;
    }

    /***
    * @return total supply of all tokens
    */
    function totalSupply() external view returns (uint256){
        return _totalSupply;
    }

    /***
    * @notice returns amount of tokens from provided user
    * @param owner address of a user
    * @return balance of a user
    */
    function balanceOf(address owner) external view returns (uint256){
        return _balances[owner];
    }

    /***
    * @notice transfers tokens from current user to provided one
    * @param to address of the receiver
    * @param value amount of tokens to be transfered
    * @return `true` if the transfer was successful, otherwise false
    */
    function transfer(address to, uint256 value) external returns (bool){
        require(value <= _balances[msg.sender]);
        require(to != address(0));

        _balances[msg.sender] = _balances[msg.sender].sub(value);
        _balances[to] = _balances[to].add(value);
        emit Transfer(msg.sender, to, value);

        return true;
    }

    /***
    * @notice transfers tokens from provided user to provided one, the current user must be allowance (using approve)
    * by the owner to spend his tokens
    * @param from address of the sender
    * @param to address of the receiver
    * @param value amount of tokens to be transferred
    * @return `true` if the transfer was successful, otherwise false
    */
    function transferFrom(address from, address to, uint256 value) external returns (bool success){
        require(value <= _allowed[from][msg.sender]);
        _allowed[from][msg.sender] = _allowed[from][msg.sender].sub(value);

        _balances[from] = _balances[from].sub(value);
        _balances[to] = _balances[to].add(value);
        emit Transfer(from, to, value);

        return true;
    }

    /***
    * @notice allowance a user to spend provided amount of own tokens
    * @param spender address of the user that is allowed to spend own tokens
    * @param value amount of tokens he is allowed to spend
    * @return `true` if the transfer was successful, otherwise false
    * @dev be careful with updating allowances, because it might be that the spender has access to the old and new
    *  amounts of tokens. For more information visit https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
    */
    function approve(address spender, uint256 value) external returns (bool success){
        require(spender != address(0));

        _allowed[msg.sender][spender] = value;
        emit Approval(msg.sender, spender, value);
        return true;
    }

    /***
    * @notice get number of allowances for a user
    * @param owner address of the owner of the tokens
    * @param spender address of the user that is allowed to spend the owner's tokens
    * @return number of tokens the spender is allowed to spend
    */
    function allowance(address owner, address spender) external view returns (uint256 remaining){
        return _allowed[owner][spender];
    }
}