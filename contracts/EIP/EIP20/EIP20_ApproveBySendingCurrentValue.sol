pragma solidity ^0.4.23;

import "./EIP20_Default.sol";
import "./SafeMath.sol";

/// https://eips.ethereum.org/EIPS/eip-20
/// based on implementation of OpenZeppelin: https://github.com/OpenZeppelin/openzeppelin-solidity/blob/master/contracts/token/EIP20/EIP20.sol
contract EIP20_ApproveBySendingCurrentValue is EIP20_Default {
    using SafeMath for uint256;

    constructor(string name, string symbol, uint8 decimals, uint256 totalSupply) EIP20_Default(name, symbol, decimals, totalSupply) internal {

        //pre-calculated interfaceHashes are used to reduce gas
//        registerInterface(
//            calculateFunctionHash("name()") ^
//            calculateFunctionHash("symbol()") ^
//            calculateFunctionHash("decimals()") ^
//            calculateFunctionHash("totalSupply()") ^
//            calculateFunctionHash("balanceOf(address)") ^
//            calculateFunctionHash("transfer(address,value)") ^
//            calculateFunctionHash("transferFrom(address,address,value)") ^
//            calculateFunctionHash("approve(address,uint256,uint256)") ^
//            calculateFunctionHash("allowance(address,address)");
        registerInterface(0x7cc24d34);
    }

    /***
    * @notice allowance a user to spend provided amount of own tokens
    * @param spender address of the user that is allowed to spend own tokens
    * @param currentValue current amount of tokens he is allowed to spend
    * @param value amount of tokens he is allowed to spend
    * @return `true` if the transfer was successful, otherwise false
    * @dev this is a fix of the race condition provided in
    *  https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
    */
    function approve(address spender, uint256 currentValue, uint256 value) external returns (bool success){
        require(spender != address(0));
        require(0 == _allowed[msg.sender][spender] - currentValue);

        _allowed[msg.sender][spender] = value;
        emit Approval(msg.sender, spender, value);
        return true;
    }
}