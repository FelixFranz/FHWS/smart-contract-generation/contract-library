pragma solidity ^0.4.23;

import "./EIP20_Default.sol";
import "./SafeMath.sol";

/// https://eips.ethereum.org/EIPS/eip-20
/// based on implementation of OpenZeppelin: https://github.com/OpenZeppelin/openzeppelin-solidity/blob/master/contracts/token/EIP20/EIP20.sol
contract EIP20_ApproveByAddAllowance is EIP20_Default {
    using SafeMath for uint256;

    constructor(string name, string symbol, uint8 decimals, uint256 totalSupply) EIP20_Default(name, symbol, decimals, totalSupply) internal {
        //pre-calculated interfaceHashes are used to reduce gas
        //        registerInterface(
        //            calculateFunctionHash("name()") ^
        //            calculateFunctionHash("symbol()") ^
        //            calculateFunctionHash("decimals()") ^
        //            calculateFunctionHash("totalSupply()") ^
        //            calculateFunctionHash("balanceOf(address)") ^
        //            calculateFunctionHash("transfer(address,value)") ^
        //            calculateFunctionHash("transferFrom(address,address,value)") ^
        //            calculateFunctionHash("approve(address,uint256,bool)") ^
        //            calculateFunctionHash("addApprove(address,uint256)") ^
        //            calculateFunctionHash("removeApprove(address,uint256)") ^
        //            calculateFunctionHash("allowance(address,address)"));
        registerInterface(0x6ea3a097);
    }

    /***
    * @notice add allowance to a user to spend provided amount of own tokens. To reduce allowance add needs to be false
    * @param spender address of the user that is allowed to spend own tokens
    * @param value amount of tokens he is allowed to spend
    * @param add true: add value of allowed tokens, false: remove value of allowed tokens
    * @return `true` if the transfer was successful, otherwise false
    * @dev this is a fix of the race condition provided in
    *  https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
    */
    function approve(address spender, uint256 value, bool add) public returns (bool success){
        require(spender != address(0));
        uint256 calculated;
        if (add)
            calculated = _allowed[msg.sender][spender].add(value);
        else
            calculated = _allowed[msg.sender][spender].sub(value);
        //require(0 <= calculated);


        _allowed[msg.sender][spender] = calculated;
        emit Approval(msg.sender, spender, calculated);
        return true;
    }

    /***
    * @notice add allowance to a user to spend provided amount of own tokens
    * @param spender address of the user that is allowed to spend own tokens
    * @param value amount of tokens he is allowed to spend
    * @return `true` if the transfer was successful, otherwise false
    */
    function addApprove(address spender, uint256 value) external returns (bool success){
        return approve(spender, value, true);
    }

    /***
    * @notice removes allowance from a user to spend provided amount of own tokens
    * @param spender address of the user that is allowed to spend own tokens
    * @param value amount of tokens that will be removed from spender's allowances
    * @return `true` if the transfer was successful, otherwise false
    */
    function removeApprove(address spender, uint256 value) external returns (bool success){
        return approve(spender, value, false);
    }
}