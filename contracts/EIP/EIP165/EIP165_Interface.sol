pragma solidity ^0.4.23;

/// https://eips.ethereum.org/EIPS/eip-165
interface EIP165_Interface {
    /// @notice Query if a contract implements an interface
    /// @param interfaceID The interface identifier, as specified in EIP-165
    /// @dev Interface identification is specified in EIP-165. This function
    ///  uses less than 30,000 gas.
    /// @return `true` if the contract implements `interfaceID` and
    ///  `interfaceID` is not 0xffffffff, `false` otherwise
    function supportsInterface(bytes4 interfaceID) external view returns (bool);
}