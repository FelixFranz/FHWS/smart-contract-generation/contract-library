pragma solidity ^0.4.23;

import "./EIP165_Interface.sol";

/// https://eips.ethereum.org/EIPS/eip-165
contract EIP165_Default is EIP165_Interface {

    /// Stores unique identifier of each super contract/interface and the current contract as key
    mapping(bytes4 => bool) private _supportedInterfaces;
    bytes4 private _supportedInterfacesAll = 0x00000000;

    /// To support EIP165 every function need to be added to the supportedInterfaces mapping
    /// single function: supportedInterfaces[this.foo.selector] = true;
    /// multiple functions: registerInterface(calculateFunctionHash("foo(bytes4)") ^ calculateFunctionHash("bar()"));
    constructor() internal {
        //pre-calculated interfaceHashes are used to reduce gas
//        registerInterface(calculateFunctionHash("supportsInterface(bytes4)"));
        registerInterface(0x01ffc9a7);
//        registerInterface(
//            calculateFunctionHash("supportsInterface(bytes4)") ^
//            calculateFunctionHash("registerInterface(bytes4)") ^
//            calculateFunctionHash("calculateFunctionHash(string)"));
        registerInterface(0xf19c67b1);
    }

    /// @notice Query if a contract implements an interface
    /// @param interfaceID The interface identifier, as specified in EIP-165
    /// @dev Interface identification is specified in EIP-165. This function
    ///  uses less than 30,000 gas.
    /// @return `true` if the contract implements `interfaceID` and
    ///  `interfaceID` is not 0xffffffff, `false` otherwise
    function supportsInterface(bytes4 interfaceID) public view returns (bool) {
        if (bytes4(0xffffffff) == interfaceID)
            return false;
        if (_supportedInterfaces[interfaceID])
            return true;
        return _supportedInterfacesAll == interfaceID;
    }

    /// @notice To add new interface to the supportedInterfaces mapping
    /// @dev If you have multiple functions inside an interface you need to binary or them. To generate the string you
    ///  can use calculateFunctionHash function:
    function registerInterface (bytes4 interfaceId) internal{
        require(bytes4(0xffffffff) != interfaceId);
        _supportedInterfaces[interfaceId] = true;
        _supportedInterfacesAll ^= interfaceId;
    }

    /// @notice calculates a function hash from a function string to be inserted into supportedInterfaces
    ///  using registerInterface function
    /// @dev The function string need to be following: functionName(datatypeOfParameter,comma,separated,without,spaces)
    /// @param functionString Function string to be converted
    /// @return functionHash that describes the function
    function calculateFunctionHash (string functionString) public pure returns (bytes4){
        return bytes4(keccak256(abi.encodePacked(functionString)));
    }
}