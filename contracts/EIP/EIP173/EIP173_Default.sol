pragma solidity ^0.4.23;

import "../EIP165/EIP165_Default.sol";
import "./EIP173_Interface.sol";

/// https://eips.ethereum.org/EIPS/eip-173
contract EIP173_Default is EIP165_Default, EIP173_Interface {

    address _owner;

    constructor() internal{
        _owner = msg.sender;
        //pre-calculated interfaceHashes are used to reduce gas
        //        registerInterface(
        //            calculateFunctionHash("owner()") ^
        //            calculateFunctionHash("transferOwnership(address)"));
        registerInterface(0x7f5828d0);
    }

    /// @notice Get the address of the owner
    /// @return The address of the owner.
    function owner() view external returns (address){
        return _owner;
    }

    /// @notice Set the address of the new owner of the contract
    /// @param newOwner The address of the new owner of the contract
    function transferOwnership(address newOwner) external{
        require(_owner == msg.sender);
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner,  newOwner);
    }
}