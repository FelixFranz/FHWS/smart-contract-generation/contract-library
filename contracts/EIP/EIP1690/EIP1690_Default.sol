pragma solidity ^0.4.23;

import "../EIP165/EIP165_Default.sol";
import "../EIP173/EIP173_Default.sol";
import "./EIP1690_Interface.sol";

/// https://github.com/ethereum/EIPs/blob/d4c18eb0d34ddf2f90e0eccb6be09accd7b1f086/EIPS/eip-1690.md
contract EIP1690_Default is EIP165_Default, EIP173_Default, EIP1690_Interface {

    constructor() internal{
        //pre-calculated interfaceHashes are used to reduce gas
        //        registerInterface(calculateFunctionHash("kill()"));
        registerInterface(0x41c0e1b5);
    }

    /// function to destruct the contract and send the ether to provided address
    function kill(address receiver) public {
        require(msg.sender == _owner);
        selfdestruct(receiver);
    }

    /// function to destruct the contract and send the ether to the owner
    function kill() public{
        kill(_owner);
    }
}