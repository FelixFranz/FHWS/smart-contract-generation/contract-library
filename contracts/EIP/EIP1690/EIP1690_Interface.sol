pragma solidity ^0.4.23;

/// https://github.com/ethereum/EIPs/blob/d4c18eb0d34ddf2f90e0eccb6be09accd7b1f086/EIPS/eip-1690.md
interface EIP1690_Interface {
    /// function to destruct the contract and send the ether to provided address
    function kill(address receiver) external;

    /// function to destruct the contract and send the ether to the owner
    function kill() external;
}