pragma solidity ^0.4.23;

import "./EIP/EIP165/EIP165_Default.sol";
import "./EIP/EIP20/EIP20_Default.sol";
import "./EIP/EIP20/EIP20_ApproveBySendingCurrentValue.sol";
import "./EIP/EIP20/EIP20_ApproveByAddAllowance.sol";
import "./EIP/EIP173/EIP173_Default.sol";
import "./EIP/EIP1690/EIP1690_Default.sol";

contract Contract1 is EIP165_Default, EIP20_Default, EIP173_Default {

    constructor() EIP20_Default("TestToken", "Test", 42, 1000) public {
        //pre-calculated interfaceHashes are used to reduce gas
        //registerInterface(calculateFunctionHash("test()"));
        registerInterface(0xf8a8fd6d);
    }

    // test with truffle console: Contract1.deployed().then(instance => instance.test.call()).then(txt => console.log(txt))
    function test () public pure returns (string) {
        return "Hello World";
    }

    function calcId() public pure returns (bytes4){
        return calculateFunctionHash("kill()");
    }
}

contract Contract2 is EIP20_ApproveBySendingCurrentValue, EIP1690_Default {

    constructor() EIP20_ApproveBySendingCurrentValue("TestToken2", "Test2", 84, 2000) public {

    }
}

contract Contract3 is EIP20_ApproveByAddAllowance {

    constructor() EIP20_ApproveByAddAllowance("TestToken3", "Test3", 33, 3000) public {

    }
}