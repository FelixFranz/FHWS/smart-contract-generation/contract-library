let Contract1 = artifacts.require("./Contract1.sol");
let Contract2 = artifacts.require("./Contract2.sol");
let Contract3 = artifacts.require("./Contract3.sol");

module.exports = function(deployer) {
    deployer.deploy(Contract1);
    deployer.deploy(Contract2);
    deployer.deploy(Contract3);
};