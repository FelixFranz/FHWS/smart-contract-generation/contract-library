const interfaceID = "0x7f5828d0";

module.exports.test = function (contract, accounts, expected){
    describe("EIP-173_Default", () => {
	    it("supportsInterface: Should recognize supported interface EIP173", async () => {
		    let instance = await contract.deployed();
		    let equal = await instance.supportsInterface.call(interfaceID);
		    assert.isTrue(equal);
	    });

        it("owner: Should have the coorect initial owner", async () => {
            let instance = await contract.new();
            let owner = await instance.owner.call();
            assert.equal(accounts[0], owner, "Wrong initial owner");
        });

        it("transferOwnership: Should transfer the ownership", async () => {
            let instance = await contract.new();
            await instance.transferOwnership(accounts[1]);
            let owner = await instance.owner.call();
            assert.equal(accounts[1], owner, "Wrong new owner");
        });

        it("transferOwnership: Should not transfer the ownership if caller is not owner", async () => {
            let instance = await contract.new();
            let fail = false;
            try{
                await instance.transferOwnership(accounts[1], {from: accounts[1]});
                fail = true;
			} catch (e) {

            }
            if (fail) assert.fail("Changing owner should not be possible if owner = function sender (caller)");
        });

	    it("OwnershipTransferred: OwnershipTransferred event should be triggered by function transferOwnership", async () =>{
		    let instance = await contract.new();
		    let event = instance.OwnershipTransferred({});
		    let promise = new Promise((resolve) => {
			    event.watch((error, result) => {
				    if (error) reject();
				    assert.equal(accounts[0], result.args.previousOwner);
				    assert.equal(accounts[1], result.args.newOwner);
				    resolve();
			    });
		    });
		    setTimeout(() =>{
			    event.stopWatching();
			    assert.fail();
		    }, 60000);
		    await instance.transferOwnership(accounts[1]);
		    await promise;
		    event.stopWatching();
	    });
    });
};