module.exports.test = function (contract, accounts, expected){
    describe("EIP-20_Default", () => {

        require("./General").test(contract, accounts, expected,
            instance => instance.approve(accounts[1], 1, {from: accounts[0]}));

        it("approve: Approve should create allowances", async () =>{
            let instance = await contract.new();
            await instance.approve(accounts[1], 1, {from: accounts[0]});
            let allowance = await instance.allowance.call(accounts[0], accounts[1]);
            assert.equal(1, parseInt(allowance));
        });
    });
};