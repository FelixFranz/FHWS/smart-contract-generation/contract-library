let InterfaceID = "0x6ea3a097";

module.exports.test = function (contract, accounts, expected){
    describe("EIP-20_ApproveByAddAllowance", () => {

        it("supportsInterface: Should recognize supported interface EIP20_ApproveByAddAllowance", async () => {
            let instance = await contract.deployed();
            let equal = await instance.supportsInterface.call(InterfaceID);
            assert.isTrue(equal);
        });

        require("./General").test(contract, accounts, expected,
            instance => instance.addApprove(accounts[1], 1, {from: accounts[0]}));

        it("approve: Approve should not remove more than all available allowances", async () =>{
            let instance = await contract.new();
            let allowance = await instance.allowance.call(accounts[0], accounts[1]);
            try {
                await instance.addApprove(accounts[1], (parseInt(allowance) + 1) * -1, 1, {from: accounts[0]});
                assert.fail();
            } catch (e) {

            }
        });

        it("approve: Approve should add allowances", async () =>{
            let instance = await contract.new();
            let allowance = await instance.allowance.call(accounts[0], accounts[1]);
            await instance.addApprove(accounts[1], 1, {from: accounts[0]});
            let newAllowance = await instance.allowance.call(accounts[0], accounts[1]);
            assert.equal(parseInt(allowance) + 1, parseInt(newAllowance));
        });

        it("approve: Approve should remove allowances", async () =>{
            let instance = await contract.deployed();
	        await instance.addApprove(accounts[1], 1, {from: accounts[0]});
            let allowance = await instance.allowance.call(accounts[0], accounts[1]);
            await instance.removeApprove(accounts[1], 1, {from: accounts[0]});
            let newAllowance = await instance.allowance.call(accounts[0], accounts[1]);
            assert.equal(parseInt(allowance) - 1, parseInt(newAllowance));
        });
    });
};