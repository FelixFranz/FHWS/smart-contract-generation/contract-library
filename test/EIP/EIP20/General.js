const InterfaceID = "0x37f66e14";

module.exports.test = function (contract, accounts, expected, approveFunction){
    it("supportsInterface: Should recognize supported interface EIP20", async () => {
        let instance = await contract.deployed();
        let equal = await instance.supportsInterface.call(InterfaceID);
        assert.isTrue(equal);
    });

    it("name: Should have the correct name", async () =>{
        let instance = await contract.deployed();
        let name = await instance.name.call();
        assert.equal(expected.name, name);
    });

    it("symbol: Should have the correct symbol", async () =>{
        let instance = await contract.deployed();
        let symbol = await instance.symbol.call();
        assert.equal(expected.symbol, symbol);
    });

    it("decimals: Should have the correct decimals", async () =>{
        let instance = await contract.deployed();
        let decimals = await instance.decimals.call();
        assert.equal(expected.decimals, decimals);
    });

    it("totalSupply: Should have the correct totalSupply", async () =>{
        let instance = await contract.deployed();
        let totalSupply = await instance.totalSupply.call();
        assert.equal(expected.totalSupply, totalSupply);
    });

    it("balance: Should have the correct initial balance", async () =>{
        let instance = await contract.new();
        let balance = await instance.balanceOf.call(accounts[0]);
        assert.equal(expected.totalSupply, balance);
    });

    it("transfer: Transfer should not be possible with not enough tokens", async () =>{
        let instance = await contract.new();
        let balance0 = await instance.balanceOf.call(accounts[0]);
        let balance1 = await instance.balanceOf.call(accounts[1]);
        try {
            await instance.transfer(accounts[0], 1, {from: accounts[1]});
            assert.fail();
        } catch (e) {

        }
        let newBalance0 = await instance.balanceOf.call(accounts[0]);
        let newBalance1 = await instance.balanceOf.call(accounts[1]);
        assert.equal(parseInt(balance0), parseInt(newBalance0));
        assert.equal(parseInt(balance1), parseInt(newBalance1));
    });

    it("transfer: Transfer should be possible with enough tokens", async () =>{
        let instance = await contract.new();
        let balance0 = await instance.balanceOf.call(accounts[0]);
        let balance1 = await instance.balanceOf.call(accounts[1]);
        try {
            await instance.transfer(accounts[1], 1);
        } catch (e) {
            assert.fail()
        }
        let newBalance0 = await instance.balanceOf.call(accounts[0]);
        let newBalance1 = await instance.balanceOf.call(accounts[1]);
        assert.equal(parseInt(balance0) - 1, parseInt(newBalance0));
        assert.equal(parseInt(balance1) + 1, parseInt(newBalance1));
    });

    it("Transfer: Transfer event should be triggered by function transfer", async () =>{
        let instance = await contract.new();
        let event = instance.Transfer({});
        let promise = new Promise((resolve) => {
            event.watch((error, result) => {
                if (error) reject();
                assert.equal(accounts[0], result.args.from);
                assert.equal(accounts[1], result.args.to);
                assert.equal(1, result.args.value);
                resolve();
            });
        });
        setTimeout(() =>{
            event.stopWatching();
            assert.fail();
        }, 20000);
        await instance.transfer(accounts[1], 1);
        await promise;
        event.stopWatching();
    });

    it("transferFrom: Should only be possible if there are enough allowances", async () =>{
        let instance = await contract.new();
        try {
            await instance.transferFrom(accounts[1], accounts[0], 1);
            assert.fail();
        } catch (e) {

        }
    });

    it("transferFrom: Should be possible with enought allowances", async () =>{
        let instance = await contract.new();
        await approveFunction(instance);
        let allowance = await instance.allowance.call(accounts[0], accounts[1]);
        let balance0 = await instance.balanceOf.call(accounts[0]);
        let balance1 = await instance.balanceOf.call(accounts[1]);
        await instance.transferFrom(accounts[0], accounts[1], 1, {from: accounts[1]});
        let newAllowance = await instance.allowance.call(accounts[0], accounts[1]);
        let newBalance0 = await instance.balanceOf.call(accounts[0]);
        let newBalance1 = await instance.balanceOf.call(accounts[1]);
        assert.equal(parseInt(allowance) - 1, parseInt(newAllowance));
        assert.equal(parseInt(balance0) - 1, parseInt(newBalance0));
        assert.equal(parseInt(balance1) + 1, parseInt(newBalance1));
    });

    it("Transfer: Transfer event should be triggered by function transferFrom", async () =>{
        let instance = await contract.new();
        let event = instance.Transfer({});
        let promise = new Promise((resolve) => {
            event.watch((error, result) => {
                if (error) reject();
                assert.equal(accounts[0], result.args.from);
                assert.equal(accounts[1], result.args.to);
                assert.equal(1, result.args.value);
                resolve();
            });
        });
        await approveFunction(instance);
        await instance.transferFrom(accounts[0], accounts[1], 1, {from: accounts[1]});
        setTimeout(() =>{
            event.stopWatching();
            assert.fail();
        }, 20000);
        await promise;
        event.stopWatching();
    });

    it("Approval: Approval event should be triggered by function approve", async () =>{
        let instance = await contract.new();
        let event = instance.Approval({});
        let promise = new Promise((resolve) => {
            event.watch((error, result) => {
                if (error) reject();
                assert.equal(accounts[0], result.args.owner);
                assert.equal(accounts[1], result.args.spender);
                assert.equal(1, result.args.value);
                resolve();
            });
        });
        setTimeout(() =>{
            event.stopWatching();
            assert.fail();
        }, 60000);
        await approveFunction(instance);
        await promise;
        event.stopWatching();
    });

    it("allowance: There should be no allowance initial", async () =>{
        let instance = await contract.new();
        let allowance = await instance.allowance.call(accounts[1], accounts[0]);
        assert.equal(0, allowance);
    });
};