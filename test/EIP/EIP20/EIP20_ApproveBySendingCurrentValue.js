let InterfaceID = "0x7cc24d34";

module.exports.test = function (contract, accounts, expected){
    describe("EIP-20_ApproveBySendingCurrentValue", () => {

        it("supportsInterface: Should recognize supported interface EIP20_ApproveBySendingCurrentValue", async () => {
            let instance = await contract.deployed();
            let equal = await instance.supportsInterface.call(InterfaceID);
            assert.isTrue(equal);
        });

        require("./General").test(contract, accounts, expected,
            async instance => {
                let allowance = await instance.allowance.call(accounts[0], accounts[1]);
                // using workaround for function overloading from https://github.com/trufflesuite/truffle/issues/737#issuecomment-422708305
                await instance.contract.approve["address,uint256,uint256"](accounts[1], allowance, 1, {from: accounts[0]})
            });

        it("approve: Approve not work if a wrong current value is provided", async () =>{
            let instance = await contract.new();
            let allowance = await instance.allowance.call(accounts[0], accounts[1]);
            try {
                // using workaround for function overloading from https://github.com/trufflesuite/truffle/issues/737#issuecomment-422708305
                await instance.contract.approve["address,uint256,uint256"](accounts[1], parseInt(allowance) + 1, 1, {from: accounts[0]});
                assert.fail();
            } catch (e) {

            }
        });

        it("approve: Approve should create allowances", async () =>{
            let instance = await contract.new();
            let allowance = await instance.allowance.call(accounts[0], accounts[1]);
            // using workaround for function overloading from https://github.com/trufflesuite/truffle/issues/737#issuecomment-422708305
            await instance.contract.approve["address,uint256,uint256"](accounts[1], allowance, 1, {from: accounts[0]});
            allowance = await instance.allowance.call(accounts[0], accounts[1]);
            assert.equal(1, parseInt(allowance));
        });
    });
};