const InvalidID = "0xffffffff";
const EIP165InterfaceID = "0x01ffc9a7";
const EIP165DefaultImplementationID = "0xf19c67b1";

module.exports.test = function (contract, accounts){
    describe("EIP-165_Default", () => {
        it("supportsInterface: Should recognize supported interface EIP165", async () => {
            let instance = await contract.deployed();
            let equal = await instance.supportsInterface.call(EIP165InterfaceID)
            assert.isTrue(equal);
        });

        it("supportsInterface: Should recognize extended version of supported interface EIP165", async () => {
            let instance = await contract.deployed();
            let equal = await instance.supportsInterface.call(EIP165DefaultImplementationID)
            assert.isTrue(equal);
        });

        it("supportsInterface: Should recognize an invalid interface id", async () => {
            let instance = await contract.deployed();
            let equal = await instance.supportsInterface.call(InvalidID)
            assert.isFalse(equal);
        });
    });
};