const interfaceID = "0x41c0e1b5";

module.exports.test = function (contract, accounts, expected){
    require("../EIP173/EIP173_Default").test(contract, accounts, {});
    
    describe("EIP-1690_Default", () => {
        it("supportsInterface: Should recognize supported interface EIP1690", async () => {
            let instance = await contract.deployed();
            let equal = await instance.supportsInterface.call(interfaceID);
            assert.isTrue(equal);
        });

        it("kill: Should kill the Smart Contract", async () => {
            let instance = await contract.new();
            await instance.kill();
            let fail = false;
            try {
                await instance.kill();
                fail = true;
            } catch (e) {
                if (e.message !== "Attempting to run transaction which calls a contract function, but recipient address "
                    + instance.address + " is not a contract address")
                    assert.fail("Thrown wrong error message!");
            }
            if (fail) assert.fail("An error need to be thrown");
        });

        it("kill: Should not kill the Smart Contract if the user is not allowed", async () => {
            let instance = await contract.new();
            let fail = false;
            try {
                await instance.kill({from: accounts[1]});
                fail = true;
            } catch (e) {

            }
            if (fail) assert.fail("An error need to be thrown");
        });

        it("kill(receiver): Should kill the Smart Contract", async () => {
            let instance = await contract.new();
            await instance.kill();
            let fail = false;
            try {
                // await instance.kill(accounts[2]);
                // using workaround for function overloading from https://github.com/trufflesuite/truffle/issues/737#issuecomment-422708305
                await instance.contract.kill["address"](accounts[2], {from: accounts[0]});
                fail = true;
            } catch (e) {
                if (e.message !== "Attempting to run transaction which calls a contract function, but recipient address "
                    + instance.address + " is not a contract address")
                    assert.fail("Thrown wrong error message!");
            }
            if (fail) assert.fail("An error need to be thrown");
        });

        it("kill(receiver): Should not kill the Smart Contract if the user is not allowed", async () => {
            let instance = await contract.new();
            let fail = false;
            try {
                // await instance.kill(accounts[2], {from: accounts[1]});
                // using workaround for function overloading from https://github.com/trufflesuite/truffle/issues/737#issuecomment-422708305
                await instance.contract.kill["address"](accounts[2], {from: accounts[1]});
                fail = true;
            } catch (e) {

            }
            if (fail) assert.fail("An error need to be thrown");
        });
    });
};