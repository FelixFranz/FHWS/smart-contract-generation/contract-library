let Contract1 = artifacts.require("./Contract1.sol");
let Contract2 = artifacts.require("./Contract2.sol");
let Contract3 = artifacts.require("./Contract3.sol");

//var c1; Contract1.deployed().then(t => c1=t);
//var c2; Contract2.deployed().then(t => c2=t);
//var c2; Contract3.deployed().then(t => c3=t);

contract(Contract1, (accounts) => {
    require("./EIP/EIP165/EIP165_Default").test(Contract1, accounts);
    require("./EIP/EIP20/EIP20_Default").test(Contract1, accounts, {
        name: "TestToken",
        symbol: "Test",
        decimals: 42,
        totalSupply: 1000
    });
	require("./EIP/EIP173/EIP173_Default").test(Contract1, accounts, {});
});

contract(Contract2, (accounts) => {
    require("./EIP/EIP20/EIP20_ApproveBySendingCurrentValue").test(Contract2, accounts, {
        name: "TestToken2",
        symbol: "Test2",
        decimals: 84,
        totalSupply: 2000
    });
    require("./EIP/EIP1690/EIP1690_Default").test(Contract2, accounts, {});
});

contract(Contract3, (accounts) => {
    require("./EIP/EIP20/EIP20_ApproveByAddAllowance").test(Contract3, accounts, {
        name: "TestToken3",
        symbol: "Test3",
        decimals: 33,
        totalSupply: 3000
    });
});